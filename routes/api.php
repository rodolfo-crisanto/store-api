<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');

   

Route::middleware('auth:api')->group( function () {

    // Products
    Route::get('products', 'API\ProductController@index');
    Route::get('products/{product}', 'API\ProductController@show');
    Route::post('products', 'API\ProductController@store');
    Route::put('products/{product}', 'API\ProductController@update');
    Route::delete('products/{product}', 'API\ProductController@destroy');

    // Currencies
    Route::get('currencies', 'API\CurrencyController@index');
    Route::get('currencies/{currency}', 'API\CurrencyController@show');
    Route::post('currencies', 'API\CurrencyController@store');
    Route::put('currencies/{currency}', 'API\CurrencyController@update');
    Route::delete('currencies/{currency}', 'API\CurrencyController@destroy');

    // Categories
    Route::get('categories', 'API\CategoryController@index');
    Route::post('categories', 'API\CategoryController@store');

    // Logout
    Route::post('logout', 'API\RegisterController@logout');

});