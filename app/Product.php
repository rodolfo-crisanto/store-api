<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'image', 'name', 'description', 'price', 'currencies_id', 'discount', 'offer', 'categories'
    ];
}
