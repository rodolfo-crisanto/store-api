<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'name', 'abbreviation', 'default'
    ];
}
