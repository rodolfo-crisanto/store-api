<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Requests\API\CurrencyRequest;
use App\Currency;
use App\Http\Resources\Currency as CurrencyResource;

class CurrencyController extends BaseController
{
    public function index()
    {
        $currencies = Currency::all();
        return $this->sendResponse(CurrencyResource::collection($currencies), 'Currencies retrieved successfully.');
    }

    public function store(CurrencyRequest $request)
    {
        try {
            $currency = Currency::create($request->all());
            if($request->default) {
                $this->restoreDefault($currency->id);
            }
            return $this->sendResponse(new CurrencyResource($currency), trans('custom.message.create.success', ['name' => trans('custom.attribute.currency')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }

    public function show($id)
    {
        $currency = Currency::find($id);

        if (is_null($currency)) {
            return $this->sendError('Currency not found.');
        }

        return $this->sendResponse(new CurrencyResource($currency), 'Currency retrieved successfully.');
    }

    public function update(CurrencyRequest $request, Currency $currency)
    {
        
        try {
            $currency->name = $request->name;
            $currency->abbreviation = $request->abbreviation;
            $currency->default = $request->default;
            $currency->save();
            if($currency->default) {
                $this->restoreDefault($currency->id);
             } 
            return $this->sendResponse(new CurrencyResource($currency), trans('custom.message.update.success', ['name' => trans('custom.attribute.currency')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }

    public function destroy(Currency $currency)
    {
        try {
            $currency->delete();
            return $this->sendResponse([], trans('custom.message.destroy.success', ['name' => trans('custom.attribute.currency')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }

    private function restoreDefault($id)
    {
        try {
            Currency::where('_id', '!=', $id)
                    ->update([ 'default' => false ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
