<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\ProductRequest;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Product;
use Illuminate\Support\Facades\Storage;
use MongoDB\BSON\ObjectID;
use App\Http\Resources\Product as ProductResource;

class ProductController extends BaseController
{
    public function index()
    {
        $products = Product::all();
        return $this->sendResponse(ProductResource::collection($products), 'Products retrieved successfully.');
    }

    public function store(ProductRequest $request)
    {
        try {
            $data = $request->all();
            if ($request->hasFile('imageProduct')) {
                $path = $request->file('imageProduct')->store('public/products');
                $data['image'] = $path;
            }
            $data['currencies_id'] = '';
            $data['currencies_id'] = new ObjectID($request->currencies_id);
            $product = Product::create($data);
            return $this->sendResponse(new ProductResource($product), trans('custom.message.create.success', ['name' => trans('custom.attribute.product')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }

    public function show($id)
    {
        $product = Product::find($id);

        if (is_null($product)) {
            return $this->sendError('Product not found.');
        }

        return $this->sendResponse(new ProductResource($product), 'Product retrieved successfully.');
    }

    public function update(ProductRequest $request, Product $product)
    {
        try {
            $product->name = $request->name;
            $product->detail = $request->detail;
            $product->save();

            return $this->sendResponse(new ProductResource($product), trans('custom.message.update.success', ['name' => trans('custom.attribute.product')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return $this->sendResponse([], trans('custom.message.destroy.success', ['name' => trans('custom.attribute.product')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }
}
