<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CategoryRequest;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Category;
use App\Http\Resources\Category as CategoryResource;

class CategoryController extends BaseController
{
    public function index()
    {
        $categories = Category::all();
        return $this->sendResponse(CategoryResource::collection($categories), 'Categories retrieved successfully.');
    }

    public function store(CategoryRequest $request)
    {
        try {
            $category = Category::create($request->all());
            return $this->sendResponse(new CategoryResource($category), trans('custom.message.create.success', ['name' => trans('custom.attribute.category')]));
        } catch (\Exception $e) {
            return $this->sendError('Error.', $e->getMessage());
        }
    }
}
