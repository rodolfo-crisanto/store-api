<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'              => 'required',
            'description'       => 'required', 
            'price'             => 'required|regex:/^\d*(\.\d{1,2})?$/', 
            'currencies_id'     => 'required' 
        ];
        return $rules;

    }
}
