<?php
return [
    'message' => [
        'create' => [
            'success'   => ':name created successfully.',
            'error'     => "We are sorry. :name Could not created due to error..",
        ],
        'disable' => [
            'success'   => ':name disabled successfully.',
            'error'     => "We are sorry. :name nCould not disabled due to error..",
        ],
        'update' => [
            'success'   => ':name updated successfully.',
            'error'     => "We are sorry. :name Could not updated due to error.r.",
        ],
        'destroy' => [
            'success'   => ':name deleted successfully.',
            'error'     => "We are sorry. :name Could not deleted due to error..",
        ]
    ],
    'attribute' => [
        'product'   => 'Product',
        'category'  => 'Category',
        'currency'  => 'Currency'
    ],
];
